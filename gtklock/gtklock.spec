%global forgeurl https://github.com/jovanlanik/gtklock

Name:           gtklock
Version:        4.0.0
%forgemeta
Release:        1%{?dist}
Summary:        GTK-based lockscreen for Wayland

License:        GPLv3
URL:            %{forgeurl}
Source0:        %{forgesource}
Patch0:         %{forgeurl}/pull/90.patch

BuildRequires:  meson
BuildRequires:  scdoc
BuildRequires:  gcc
BuildRequires:  /usr/bin/wayland-scanner
BuildRequires:  pkgconfig(pam)
BuildRequires:  pkgconfig(wayland-client)
BuildRequires:  pkgconfig(gtk+-wayland-3.0)
BuildRequires:  pkgconfig(gtk-session-lock-0)
BuildRequires:  pkgconfig(gmodule-export-2.0)

%description
%{summary}


%prep
%forgeautosetup -p1

%build
%meson
%meson_build

%install
%meson_install
install -dm0755 %{buildroot}%{_libdir}/%{name}

%check


%files
%{_bindir}/%{name}
%config(noreplace) %{_pam_confdir}/%{name}
%{_mandir}/man1/%{name}.1*
%{_libdir}/%{name}
%license LICENSE
%doc README.md


%changelog
* Tue Oct 22 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 4.0.0-1
- Update to 4.0.0

* Sun Apr 21 2024 Zephyr Lykos <fedora@mochaa.ws> - 3.0.0-2
- Install plugins to libdir

* Fri Apr 19 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 3.0.0-1
- Update to 3.0.0

* Mon May 29 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 2.1.0-1
- Update to 2.1.0

* Sun Mar 19 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 2.0.1-3
- Fixed linter warnings

* Sat Dec 31 2022 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 2.0.1-2
- Added directory placeholder for modules

* Fri Dec 30 2022 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 2.0.1-1
- Initial build
