%global   forgeurl https://github.com/ErikReider/SwaySettings

Name:           sway-settings
Version:        0.5.0
%forgemeta
Release:        1%{?dist}
Summary:        A gui for setting sway wallpaper, default apps, GTK themes, etc...

License:        GPLv3
URL:            %{forgeurl}
Source0:        %{forgesource}
Patch0:         %{name}.app-description.patch

Provides:       SwaySettings = %{version}
Provides:       swaysettings = %{version}

BuildRequires:  meson
BuildRequires:  vala
BuildRequires:  /usr/bin/desktop-file-validate
BuildRequires:  /usr/bin/appstream-util
BuildRequires:  /usr/bin/msgfmt
BuildRequires:  pkgconfig(gtk+-3.0)
BuildRequires:  pkgconfig(gio-2.0)
BuildRequires:  pkgconfig(gdk-pixbuf-2.0)
BuildRequires:  pkgconfig(cairo)
BuildRequires:  pkgconfig(libhandy-1)
BuildRequires:  pkgconfig(json-glib-1.0)
BuildRequires:  pkgconfig(gee-0.8)
BuildRequires:  pkgconfig(granite)
BuildRequires:  pkgconfig(accountsservice) >= 22.04
BuildRequires:  pkgconfig(gtk-layer-shell-0)
BuildRequires:  pkgconfig(libpulse)
BuildRequires:  pkgconfig(libxml-2.0)

Requires:       bluez
Requires:       xkeyboard-config

%description
%{summary}

%prep
%forgeautosetup -p1


%build
%meson
%meson_build

%install
%meson_install


%check


%files
%license LICENSE
%doc README.md
%{_bindir}/*
%{_datadir}/appdata/*
%{_datadir}/applications/*
%{_datadir}/glib-2.0/schemas/*
%{_datadir}/icons/hicolor/scalable/apps/*
%{_datadir}/icons/hicolor/symbolic/apps/*


%changelog
* Sun Dec 15 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.0-1
- Update to 0.5.0

* Mon Jan 02 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.4.0-1
- Initial build
