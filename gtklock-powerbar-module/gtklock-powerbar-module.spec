%global forgeurl https://github.com/jovanlanik/gtklock-powerbar-module

Name:           gtklock-powerbar-module
Version:        4.0.0
%forgemeta
Release:        1%{?dist}
Summary:        Gtklock module adding power controls to the lockscreen

License:        GPLv3
URL:            %{forgeurl}
Source0:        %{forgesource}
Patch0:         %{forgeurl}/pull/5.patch

BuildRequires:  make
BuildRequires:  gcc
BuildRequires:  pkgconfig(gtk+-3.0)

Requires:       gtklock%{?_isa} >= 3.0.0

Supplements:    gtklock%{?_isa}

%description
%{summary}


%prep
%forgeautosetup -p1

%build
%set_build_flags
%make_build LIBDIR="%{_libdir}"

%install
%make_install LIBDIR="%{_libdir}"


%files
%{_libdir}/gtklock/*.so
%license LICENSE
%doc README.md


%changelog
* Tue Oct 22 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 4.0.0-1
- Update to 4.0.0

* Sun Apr 21 2024 Zephyr Lykos <fedora@mochaa.ws> - 3.0.0-2
- install plugins to libdir

* Fri Apr 19 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 3.0.0-1
- Update to 3.0.0

* Sun Mar 19 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 2.0.1-2
- Fixed linter complains

* Sat Dec 31 2022 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 2.0.1-1
- Initial build
