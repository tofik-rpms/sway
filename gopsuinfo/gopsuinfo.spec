%global debug_package %{nil}

Version:        0.1.9
%global forgeurl https://github.com/nwg-piotr/gopsuinfo
%forgemeta

Name:           gopsuinfo
Release:        1%{?dist}
Summary:        Displays customizable system usage info

License:        BSD
URL:            %{forgeurl}
Source0:        %{forgesource}

BuildRequires:  go
BuildRequires:  git


%description
This program is a part of the nwg-shell project.
%{summary}

%prep
%autosetup


%build
%{__make} get
export CGO_LDFLAGS="${LDFLAGS}"
export CGO_CFLAGS="${CFLAGS}"
export CGO_CPPFLAGS="${CPPFLAGS}"
export CGO_CXXFLAGS="${CXXFLAGS}"
export GOFLAGS="-buildmode=pie -trimpath -mod=readonly -modcacherw -ldflags=-linkmode=external"
go build -o bin/%{name} *.go

%install
%make_install


%check


%files
%{_bindir}/*
%{_datadir}/%{name}
%license LICENSE


%changelog
* Wed Nov 06 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.1.9-1
- Update to 0.1.9

* Sat Oct 19 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.1.8-1
- Update to 0.1.8

* Thu Oct 17 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.1.7-1
- Update to 0.1.7

* Sun Jan 28 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.1.6-1
- Update to 0.1.6

* Sat Jun 17 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.1.5-1
- Update to 0.1.5

* Mon May 29 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.1.4-1
- Update to 0.1.4

* Fri Dec 30 2022 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.1.2-1
- Initial build
